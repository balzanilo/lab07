package it.unibo.oop.lab.nesting2;

import java.util.ArrayList;
import java.util.List;

public class OneListAcceptable<T> implements Acceptable<T> {
	
	private List<T> list = new ArrayList<>();

	@Override
	public Acceptor<T> acceptor() {
		return new Acceptor<T>() {
			private int count=0;
			
			@Override
			public void accept(T newElement) throws ElementNotAcceptedException {
				if (count < list.size() && list.get(count).equals(newElement)) {
					count ++;
				} else {
					throw new ElementNotAcceptedException(newElement);
				}
				
			}

			@Override
			public void end() throws EndNotAcceptedException {
				if (!(count==list.size())) {
					throw new EndNotAcceptedException();
				}
			}
		};
	}
	
	public OneListAcceptable(List<T> list) {
		this.list=list;
	}
}
